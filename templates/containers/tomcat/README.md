## Variables
```
{{ fedora_version }} 
```
Version of Fedora OS to be used.
```
{{ java_version }} 
```
Specific JDK version to install.
```
{{ tomcat_major_version }} 
```
Major version of the application server.
```
{{ tomcat_version }}  
```
Minor version 
```
{{ wars }}
``` 
Loop with the actual values you wish to use.

## Template Walkthrough
Here's a brief explanation of what each part does:

```
FROM fedora:{{ fedora_version }} 
```
This line specifies the base image for the Docker container, which in this case is a version of Fedora that you need to specify when rendering the template.
```
RUN dnf ... 
```
Installs OpenJDK and Tomcat using Fedora's package manager (dnf). Versions are parameterized.
```
ENV: 
```
Sets environment variables used by Tomcat.
```
EXPOSE 8080
```
Exposes port 8080 from the container to the host.
```
COPY {{ war.source }} ... 
```
This is a loop that will copy WAR files from the host into the container's webapps directory. The {{ wars }} variable should be a list of objects with source and dest attributes that you provide when rendering the template.
```
CMD ["catalina.sh", "run"] 
```
The default command to run when the container starts, which in this case is to start Tomcat.
