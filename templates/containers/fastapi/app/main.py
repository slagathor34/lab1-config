# main.py
from fastapi import FastAPI

app = FastAPI()

@app.get("/breeds")
def get_breeds():
    return {"breeds": ["Bulldog", "Dachshund", "Poodle", "Labrador"]}
